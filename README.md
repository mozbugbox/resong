ReSong 再歌
===========

Repeat a segment of song playing by lyrics.

Copyright(c) 2014 <mozbugbox@yahoo.com.au>

Released under GNU GPL version 3.0 or later.

ReSong helps you to learn new songs.

It can rewind the song play according to natural break of lyrics by looking
into a lyric file.

The lyric format are in *lrc* and can be downloaded from the net.

The local cache of the lyrics are stored in `~/.local/share/lyrics/` by defaul.

The naming convention for the cache files is `artist - title.lrc`. If you have
a better matching lyric of a song, you can save the lyric as a cache file for
the song for better resong experience.


Dependency
==========

 * Music players that support the [MPRIS protocol][1]

 * python 2.7 (parsley does not support python3)
 * configparser (configparser of Python3 backported to Python2)
 * parsley
 * pygobject (optional for GUI)
 * pycairo (optional for GUI)

[1]: <http://www.freedesktop.org/wiki/Specifications/mpris-spec/>

Installation
============

Once unpacked the source archive, type

    $ python setup.py install


Usage
=====

```sh
$ resong -h
usage: resong [-h] [--version] [-s STEP] [--padding PADDING] [-r REPEAT] [-R]
              [-l LYRIC_DIR] [-c CONFIG_FILE] [-p PLAYER] [-g] [--verbose]
              [-D]

ReSong rewind playing song by its lyric

optional arguments:
  -h, --help            show this help message and exit
  --version             show program's version number and exit
  -s STEP, --step STEP  number of sentence to step forward or backward.
                        Default: 0 (current sentence)
  --padding PADDING     extra padding time to rewind in seconds. Default: 2.00
  -r REPEAT, --repeat REPEAT
                        repeat the current line. Default: 1
  -R, --re-download     Force re-download the lyric.
  -l LYRIC_DIR, --lyric-dir LYRIC_DIR
                        root directory of the lyric file cache. Default:
                        ~/.local/share/lyrics/
  -c CONFIG_FILE, --config-file CONFIG_FILE
                        the config file to load. Default:
                        ${HOME}/.config/resong/config
  -p PLAYER, --player PLAYER
                        music player to use. Type `--player help` to find out
                        available players. Default: 1st active one on dbus
  -g, --gui             Start the GUI interface
  --verbose             print some more runtime information
  -D, --debug           debug run

```

Hint: User can link *resong* commands to certain shortcut keys through their
*window manager*.

GUI
===

There is an optional graphic user interface (GUI) to the application. To start
the application with the GUI, try:

    $ resong -g

Hotkeys are:

| Key  |   Action                                      |
|------|-----------------------------------------------|
| z    |   back to the start of the current sentence   |
| x    |   back to the start of the previous sentence  |
| r    |   toggle repeat 3 times mode                  |
| q    |   quit                                        |

Moving windows: `Alt-Left_Mouse_Button` drag

Issues
======

Report problems to: <https://www.bitbucket.com/mozbugbox/resong>

Happy Singing!

