#!/usr/bin/python
# vim:fileencoding=utf-8:sw=4:et
# Parse lrc lyric format

from __future__ import print_function, unicode_literals, absolute_import
import sys
import os
import io
import logging as log

NATIVE=sys.getfilesystemencoding()

def time2sec(txt):
    """Convert time in string form like "01:20.22" to seconds in float"""
    m, _, s = txt.partition(":")
    minute = int(m)
    second = float(s)
    total = minute*60 + second
    return total

class LrcParser:
    lrc_grammar = r"""
hexdigit = :x ?(x in '0123456789abcdefABCDEF') -> x
escapedUnicode = 'u' <hexdigit{4}>:hs -> unichr(int(hs, 16))
escapedChar = '\\' (('"' -> '"')    |('\\' -> '\\')
                   |('/' -> '/')    |('b' -> '\b')
                   |('f' -> '\f')   |('n' -> '\n')
                   |('r' -> '\r')   |('t' -> '\t')
                   |('[' -> '[')    |(']' -> ']')
                   |('\'' -> '\'')  | escapedUnicode)

newline = ('\n'|'\n\r'|'\r\n')
nbsp = (' '|'\t')* # non-break space, actually space except newline

# milli-second could be skipped
time = <digit{2} digit* ':' digit{2} ('.' digit{2})?>:t -> time2sec(t)

line_time = '[' time:time ']' -> time
line_times = line_time:first (nbsp line_time)*:rest -> [first] + rest

# <time1>word1<time2>word2<time3>word3
word_time = '<' time:time '>' -> time
word = word_time:time <(~word_time ~newline anything)+>:word ?(
    not word.isspace()) -> (time, word)
words = (word:first (~line_time word)*:rest word_time:end nbsp
    -> [first] + rest + [(end, u'')])

lyric_line = line_times:time  (
          nbsp words:words nbsp -> (u'lyric', time, words)
        | <(~newline anything)*>:line -> (u'lyric', time, line)
        )

tag_name = <letter letterOrDigit*>
tag_value = (escapedChar | ~']' anything)*:c  -> ''.join(c)
tag = (~line_time '[' tag_name:name ':' tag_value:value ']')
      -> (u'tag', name, value)

random_text = (<~newline anything>+):text -> (u"random", text)
lrc = ws ((lyric_line|tag|random_text):aline nbsp newline* ->aline)+:lines ws -> lines
"""
    def __init__(self):
        import parsley
        self.parser = parsley.makeGrammar(self.lrc_grammar,
                {"time2sec": time2sec})

    def parse(self, fname, encoding="UTF-8"):
        """Parse a lrc file with optional encoding

        Return a dict with key: "tags" and "lyrics".
        tags: a dict for tag values
        lyrics: a list of tuple for lyric lines in the form of
                [(time, lyric), ...]

        time: a floating number in seconds for the lyric line
        lyric: a str as lyrics OR a list of timed words in the
               form [(time1, word1), (time2, word2), ... (timeN, "")]

        """
        with io.open(fname, 'r', encoding=encoding) as fd:
            content = fd.read()
        return self.parse_s(content)

    def parse_s(self, text):
        lrc_info_raw = self._parse(text)
        lrc_info = self._restruct(lrc_info_raw)
        return lrc_info

    def _parse(self, text):
        """Return list of tuples as [(kind, info), ...]
        kind: can be "tag" or "lyric"
        tag: info is a tuple with (tag_name, tag_content)
        lyric: info is a line of lyrics as ([time1, time2, ..], [lyric content])
        time1, time2 are time in seconds for the given lyric content.
        lyric content: a str as lyric text OR a list of timed words in the
                       form [(time1, word1), (time2, word2), ... (timeN, "")]
        """
        parser = self.parser
        result = parser(text)
        lrc_info_raw = result.lrc()
        #print(lrc_info_raw)
        return lrc_info_raw

    def _restruct(self, lrc_raw):
        """Reconstruct raw lrc info result (parsley parsing data) into
           something cleaner.
        """
        tags = {}
        lyrics = []
        # Re-struct parse result to better form
        for line in lrc_raw:
            if line[0] == "tag":
                tags[line[1]] = line[2]
            elif line[0] == 'lyric':
                if isinstance(line[2], list):
                    line[2].sort(key=lambda x: x[0])
                for t in line[1]:
                    lyrics.append((t, line[2]))

        lyrics.sort(key=lambda x: x[0])
        lrc_info = {"tags": tags, "lyrics": lyrics}
        return lrc_info

def main():
    def set_stdio_encoding(enc=NATIVE):
        import codecs; stdio = ["stdin", "stdout", "stderr"]
        for x in stdio:
            obj = getattr(sys, x)
            if not obj.encoding: setattr(sys,  x, codecs.getwriter(enc)(obj))
    set_stdio_encoding()

    log_level = log.INFO
    log.basicConfig(format="%(levelname)s>> %(message)s", level=log_level)

    fname = "samples/sample.lrc"
    parser = LrcParser()
    lrc = parser.parse(fname)
    import mypprint; mypprint.pprint(lrc)

if __name__ == '__main__':
    main()

