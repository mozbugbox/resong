#!/usr/bin/python
# vim:fileencoding=utf-8:sw=4:et

from __future__ import print_function, unicode_literals, absolute_import
import sys
import os
import io
import logging as log

NATIVE=sys.getfilesystemencoding()

try:
    # 'unicode' exists, must be Python 2
    unicode = unicode
    str = str
    bytes = str
    basestring = basestring
except NameError:
    # 'unicode' is undefined, must be Python 3
    str = str
    unicode = str
    bytes = bytes
    basestring = (str,bytes)

# Print string without using \uxxxx form.
from pprint import PrettyPrinter as PP
class MyPPrinter(PP):
    def format(self, obj, context, maxlevels, level):
        ret = PP.format(self, obj, context, maxlevels, level)
        if isinstance(obj, unicode):
            txt = u"u'{}'".format(obj)
            ret = (txt, ret[1], ret[2])
        elif isinstance(obj, bytes):
            txt = b"b'{}'".format(obj)
            ret = (txt, ret[1], ret[2])
        return ret

pprint = MyPPrinter().pprint

def main():
    def set_stdio_encoding(enc=NATIVE):
        import codecs; stdio = ["stdin", "stdout", "stderr"]
        for x in stdio:
            obj = getattr(sys, x)
            if not obj.encoding: setattr(sys,  x, codecs.getwriter(enc)(obj))
    set_stdio_encoding()

    log_level = log.INFO
    log.basicConfig(format="%(levelname)s>> %(message)s", level=log_level)

if __name__ == '__main__':
    main()

