#!/usr/bin/python
# vim:fileencoding=utf-8:sw=4:et

from __future__ import print_function, unicode_literals, absolute_import
import sys
import os
import io
import logging as log
from distutils.core import setup

NATIVE=sys.getfilesystemencoding()

long_description = """
ReSong helps you to learn new songs.

It can rewind the song play according to natural break of lyrics by looking
into a lyric file.

The lyric format are in *lrc* and can be downloaded from the net.
"""

def get_version():
    """Get app version"""
    from resong.resong import __version__
    return __version__

def main():
    def set_stdio_encoding(enc=NATIVE):
        import codecs; stdio = ["stdin", "stdout", "stderr"]
        for x in stdio:
            obj = getattr(sys, x)
            if not obj.encoding: setattr(sys,  x, codecs.getwriter(enc)(obj))
    set_stdio_encoding()

    log_level = log.INFO
    log.basicConfig(format="%(levelname)s>> %(message)s", level=log_level)

    setup(name="ReSong",
          version=get_version(),
          description="Repeat song play by lyric",
          long_description=long_description,
          author="Mozbugbox",
          author_email="mozbugbox@yahoo.com.au",
          url="http://www.bitbucket.com/mozbugbox/resong",
          #FIXME: distutils sucked at unicode
          packages = [b"lyricparser", b"resong", b"resong.lrcdownloader"],
          scripts = ["scripts/resong"],
         )

if __name__ == "__main__":
    main()

