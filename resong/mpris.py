#!/usr/bin/python3
# vim:fileencoding=utf-8:sw=4:et

from __future__ import print_function, unicode_literals, absolute_import, division
import sys
import os
import io
import logging

from gi.repository import Gio

NATIVE=sys.getfilesystemencoding()

MPRIS_NAME_PREFIX = "org.mpris.MediaPlayer2"
MPRIS_PATH = "/org/mpris/MediaPlayer2"
class MPRISBus:
    def __init__(self, options={}):
        """
        @player_id: dbus name of a mpris player. e.g. audacious or vlc
        """
        player_iface = "org.mpris.MediaPlayer2.Player"
        self.options = options

        player_id = options.get("player", None)

        try:
            self.bus = Gio.bus_get_sync(Gio.BusType.SESSION, None)

            if player_id is None:
                player_bus = self.get_player_bus_name(MPRIS_NAME_PREFIX)
            else:
                players = self.list_players()
                if player_id not in players:
                    log.error("The given player is available: {}".format(player_id))
                    sys.exit(2)
                player_bus = "{}.{}".format(MPRIS_NAME_PREFIX, player_id)

            if not player_bus:
                log.info("No mpris player dbus found")
            else:
                log.debug("Using dbus: {}".format(player_bus))

            # connect to mpris
            self.player = Gio.DBusProxy.new_sync(self.bus,
                    Gio.DBusProxyFlags.NONE, None,
                    player_bus, MPRIS_PATH, player_iface, None)
        except IOError:
            log.error("Failed to connect dbus: {}".format(sys.exc_info()[1]))
            sys.exit(2)

    def get_player_bus_name(self, prefix):
        """Return the 1st bus with given prefix"""
        dbus_info = Gio.DBusProxy.new_sync(self.bus,
                Gio.DBusProxyFlags.NONE, None,
                "org.freedesktop.DBus", "/org/freedesktop/DBus",
                "org.freedesktop.DBus", None)
        bus_names = dbus_info.ListNames()
        player_bus = None
        for name in bus_names:
            if name.startswith(prefix):
                player_bus = name
                break
        if player_bus is None:
            bus_names = dbus_info.ListActivatableNames()
            for name in bus_names:
                if name.startswith(prefix):
                    player_bus = name
                    break
        return player_bus

    def list_players(self):
        """List available players on the system"""
        dbus_info = Gio.DBusProxy.new_sync(self.bus,
                Gio.DBusProxyFlags.NONE, None,
                "org.freedesktop.DBus", "/org/freedesktop/DBus",
                "org.freedesktop.DBus", None)
        bus_names0 = dbus_info.ListNames()
        bus_names1 = dbus_info.ListActivatableNames()
        bus_names = bus_names0 + bus_names1
        prefix = MPRIS_NAME_PREFIX
        plen = len(prefix)
        players = [x[plen:].lstrip(".")
                for x in bus_names if x.startswith(prefix)]
        return players

    def get_position(self):
        """Get current playing song position in seconds as float point"""
        p = self.player.get_cached_property("Position")
        pint = p.unpack()
        return pint/1000000.0

    def set_position(self, psec):
        """Go to position in seconds"""
        #current_id = "/org/mpris/MediaPlayer2/CurrentTrack"
        meta = self.get_metadata()
        current_id = meta["mpris:trackid"]
        usec = int(psec*1000000)
        self.player.SetPosition(b"(ox)", current_id, usec)

    def get_metadata(self):
        """Get the metadata of the current song"""
        data = self.player.get_cached_property("Metadata")
        data = data.unpack()
        def obj2u8(obj):
            if isinstance(obj, dict):
                result = {}
                for k, v in obj.items():
                    result[k] = obj2u8(v)
            elif isinstance(obj, bytes):
                result = obj.decode("UTF-8")
            elif isinstance(obj, list):
                result = []
                for item in obj:
                    result.append(obj2u8(item))
            else:
                result = obj
            return result
        data = obj2u8(data)

        return data

    def __cmp__(self, other):
        """Compare current play position with a given time in seconds."""
        if not isinstance(other, (float, int, long)):
            raise NotImplemented
        play_pos = self.get_position()
        return (play_pos - other)

def print_players():
    mpris = MPRISBus()
    players = mpris.list_players()
    print("Available Players:")
    for p in players:
        print("  " + p)

def setup_log(log_level=None):
    global log
    rlog = logging.getLogger()
    if __name__ == "__main__" and not rlog.hasHandlers():
        # setup root logger
        ch = logging.StreamHandler()
        formatter = logging.Formatter("%(levelname)s:%(name)s:: %(message)s")
        ch.setFormatter(formatter)
        rlog.addHandler(ch)

    log = logging.getLogger(__name__)

    if log_level is not None:
        log.setLevel(log_level)
        rlog.setLevel(log_level)
setup_log()

def main():
    def set_stdio_encoding(enc=NATIVE):
        import codecs; stdio = ["stdin", "stdout", "stderr"]
        for x in stdio:
            obj = getattr(sys, x)
            if not obj.encoding: setattr(sys, x, codecs.getwriter(enc)(obj))
    set_stdio_encoding()

    log_level = logging.INFO
    setup_log(log_level)

if __name__ == '__main__':
    main()

