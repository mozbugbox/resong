#!/usr/bin/python
# vim:fileencoding=utf-8:sw=4:et

from __future__ import print_function, unicode_literals, absolute_import
import sys
import os
import io
import logging as log
import time

APPNAME = "ReSong"
__version__ = "0.1"
LYRIC_DIR = "~/.local/share/lyrics/"

from gi.repository import Gio, GObject
from . import util

__version_id__ = "{} {}".format(APPNAME, __version__)
NATIVE=sys.getfilesystemencoding()
DEFAULTS = {
        "lyric_dir": LYRIC_DIR,
        "padding": 2.0, # seconds
        "repeat": 1, # seconds
        }

class App:
    def __init__(self, options):
        from . import mpris
        self.options = options
        self.mpris = mpris.MPRISBus(options)

    def get_song_info(self):
        """Collect song info from mpris"""
        mpris = self.mpris
        player_time = mpris.get_position()
        data = mpris.get_metadata()
        #print(data);sys.exit()
        title = data["xesam:title"]
        singer = " ".join(data["xesam:artist"])
        info = {"title": title,
                "singer": singer,
                "player_time": player_time,
               }
        return info

    def repeat_current_line(self, step):
        """Repeat the current lyric line by rewind it back according
        to lrc info."""
        s_info = self.get_song_info()
        if not s_info["title"]: return
        try:
            lrc = self.load_lrc(s_info["title"], s_info["singer"])
        except IOError as e:
            log.info('Failed to load lyric file "{}": {}'.format(
                e.filename, e.strerror))
            return
        self.do_repeat(s_info, lrc, step)

    def do_repeat(self, song_info, lrc, step):
        """With the download lrc, do the actual rewind"""
        new_info = self.get_song_info()
        if not (new_info["title"] == song_info["title"]
                and new_info["singer"] == song_info["singer"]):
            return

        rewind_time = self.get_rewind_time(lrc, song_info["player_time"], step)
        self.mpris.set_position(rewind_time)
        if self.options["repeat"] > 1:
            self.repeat_rewind(rewind_time, song_info["player_time"])

    def get_rewind_time(self, lrc, player_time, step):
        """Found rewind time for the given lrc"""
        offset = 0
        if "offset" in lrc["tags"]:
            offset = int(lrc["tags"]["offset"])/1000.0
        lyric_player_time = player_time - offset
        import bisect
        lrc_times = [x[0] for x in lrc["lyrics"]]
        idx = bisect.bisect_left(lrc_times, lyric_player_time)
        if idx: idx -= 1
        if step: idx += step
        if idx < 0: idx = 0

        rewind_time = lrc["lyrics"][idx][0]
        rewind_time += offset
        if "padding" in self.options:
            rewind_time -= self.options["padding"]
            if rewind_time < 0:
                rewind_time = 0
        return rewind_time

    def load_lrc(self, title, singer):
        """load the lrc struct from @title and @singer"""
        from .lrcManager import LrcManager
        lrc_man = LrcManager(self.options)
        lrc_text = lrc_man.get(title, singer)
        if not lrc_text:return # Failed to load lyric
        import xml.sax.saxutils as saxutils
        lrc_text = saxutils.unescape(lrc_text)
        #print(lrc_text)

        import lyricparser
        lrc_parser = lyricparser.LrcParser()
        lrc = lrc_parser.parse_s(lrc_text)
        return lrc

    def repeat_rewind(self, rewind_time, player_time):
        """Run the repeating lyric a few more cycle"""
        mpris = self.mpris
        options = self.options
        data = mpris.get_metadata()
        end_pos = player_time
        if "padding" in options:
            song_len = data["mpris:length"]/1000000.0 - 0.1
            end_pos += options["padding"]
            if end_pos >= song_len:
                end_pos = song_len
        rep = options["repeat"] - 1
        self.loop_rewind(rewind_time, end_pos, rep)

    def loop_rewind(self, rewind_time, end_pos, repeat_times):
        """Run a main loop to repeat the lyric"""
        wakeup_timeout = 1
        # We must run a main loop in order to get updated dbus info
        main_loop = GObject.MainLoop()
        def repeat_play(repeat):
            """A generator to repeat rewinding"""
            for i in range(repeat):
                while self.mpris < end_pos:
                    yield True
                self.mpris.set_position(rewind_time)
                yield True # run the mainloop to update dbus cache
            main_loop.quit()
            yield False

        repeater = repeat_play(repeat_times)
        GObject.timeout_add_seconds(wakeup_timeout, repeater.next)

        # force quit after a while in case some funny thing happened
        # to the media player
        repeat_gap = end_pos - rewind_time
        quit_timeout = 60*5 + repeat_gap * repeat_gap # 3 minutes enough?
        def quit_main():
            main_loop.quit()
        GObject.timeout_add_seconds(quit_timeout, main_loop.quit)

        main_loop.run()

def load_config(cfile, arg_keys=[]):
    """Read config file."""
    sect = "main"
    config = {}
    cfile = util.fix_path(cfile)
    if not os.path.exists(cfile):
        return config

    log.debug('loading config file "{}"...'.format(cfile))
    import configparser
    conf = configparser.ConfigParser()
    with io.open(cfile, "r", encoding="UTF-8") as fd:
        conf.readfp(fd)

    option_ks = conf.options(sect)
    used = set()
    for k in arg_keys: # unify cmd args and config options: "_" -> ""
        kc = k.replace("_", "")
        if kc not in option_ks:
            continue
        v = conf.get(sect, kc).strip()
        config[k] = v
        used.add(kc)

    for k, v in conf.items(sect):
        if k not in used:
            config[k] = v

    log.debug("config: {}".format(str(config)))
    return config

def load_options(args=None):
    """Load options from cmdline args and config file"""
    options = {}
    if args is None:
        arg_parser = setup_arg_parser()
        args = arg_parser.parse_args()

    arg_keys = args.__dict__.keys()
    #arg_keys.remove("config_file")
    if args.config_file:
        cfile = args.config_file
        config = load_config(cfile, arg_keys)
        options.update(config)

    for k in arg_keys: # merge cmdline args
        v = getattr(args, k)
        if v is not None:
            options[k] = v

    if "DEFAULTS" in globals(): # Load defaults
        defaults = DEFAULTS
        for k in defaults.keys():
            if  options.get(k, None) is None:
                options[k] = defaults[k]

    return args, options

def setup_arg_parser(appname=None):
    if not appname:
        appname = os.path.basename(sys.argv[0])
        if appname.endswith(".py"):
            appname = appname[:len(appname)-3]

    import argparse
    parser = argparse.ArgumentParser(
            description="{} rewind playing song by its lyric".format(appname))
    parser.add_argument("--version", action="version",
            version=__version_id__)
    parser.add_argument("-s", "--step", type=int, default=0,
            help="number of sentence to step forward or backward."
            " Default: 0 (current sentence)")
    parser.add_argument("--padding", type=float,
            help="extra padding time to rewind in seconds."
            " Default: {:.2f}".format(DEFAULTS["padding"]))
    parser.add_argument("-r", "--repeat", type=int,
            help="repeat the current line."
            " Default: {}".format(DEFAULTS["repeat"]))
    parser.add_argument("-R", "--re-download", action="store_true",
            help="Force re-download the lyric.")
    parser.add_argument("-l", "--lyric-dir", type=str,
        help="root directory of the lyric file cache. Default: {}".format(
            DEFAULTS["lyric_dir"]))
    default_config = "${{HOME}}/.config/{}/config".format(appname.lower())
    parser.add_argument("-c", "--config-file", type=str,
            default=default_config,
            help="the config file to load. Default: {}".format(default_config))
    parser.add_argument("-p", "--player", type=str,
            help="music player to use."
            " Type `--player help` to find out available players."
            " Default: 1st active one on dbus")
    parser.add_argument("-g", "--gui", action="store_true",
            help="Start the GUI interface")

    parser.add_argument("--verbose", action="store_true",
            help="print some more runtime information")
    parser.add_argument("-D", "--debug", action="store_true",
            help="debug run")

    return parser

def main(url=None):
    parser = setup_arg_parser(APPNAME)
    args = parser.parse_args()

    def set_stdio_encoding(enc=NATIVE):
        import codecs; stdio = ["stdin", "stdout", "stderr"]
        for x in stdio:
            obj = getattr(sys, x)
            if not obj.encoding: setattr(sys,  x, codecs.getwriter(enc)(obj))
    set_stdio_encoding()

    log_level = log.INFO
    if args.debug:
        log_level = log.DEBUG
    log.basicConfig(format="%(levelname)s>> %(message)s", level=log_level)

    if args.player in {"help", "?"}:
        from . import mpris
        mpris.print_players()
        return

    _, options = load_options(args)
    # type conversion
    for k in ["repeat", "step"]:
        if k in options:
            options[k] = int(options[k])
    for k in ["padding"]:
        if k in options:
            options[k] = float(options[k])

    if args.verbose:
        print("[main]")
        for k, v in options.items():
            print("{} = {}".format(k, v))

    log.debug("options: {}".format(options))
    if args.gui == True:
        from . import resongGui
        gapp = resongGui.GApp(options)
        gapp.run()
    else:
        app = App(options)
        app.repeat_current_line(options["step"])

if __name__ == '__main__':
    main()

