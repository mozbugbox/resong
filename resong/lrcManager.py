#!/usr/bin/python3
# vim:fileencoding=utf-8:sw=4:et

from __future__ import print_function, unicode_literals, absolute_import, division
import sys
import os
import io
import logging

from . import util

NATIVE=sys.getfilesystemencoding()

class LrcManager:
    """Download and fetch lyric content"""
    def __init__(self, options):
        """Select a suitable downloader and set root directory to save lyrics"""
        self.options = options
        from .lrcdownloader.baidu import BaiduDownloader
        self.downloader = BaiduDownloader()
        self.root = util.fix_path(options["lyric_dir"])
        if not os.path.isdir(self.root):
            os.makedirs(self.root)

    def make_filename(self, title, singer):
        """Made a local lyric filename from song title and artist"""
        fname = "{} - {}.lrc".format(singer, title)
        full_path = os.path.join(self.root, fname)
        return full_path

    def get(self, title, singer):
        """Fetch lyric for a song either from local disk or from the net.
        Return lyric content as UTF-8 string
        """
        full_path = self.make_filename(title, singer)
        content = None
        if not os.path.exists(full_path) or self.options.get("re_download"):
            self.download(title, singer)
        with io.open(full_path, encoding="UTF-8") as fd:
            content = fd.read()
        return content

    def download(self, title, singer):
        """Download lyric for a song and save it to local disk"""
        content = self.downloader.download(title, singer)
        if content:
            full_path = self.make_filename(title, singer)
            with io.open(full_path, "w", encoding="UTF-8") as fdw:
                fdw.write(content)

def setup_log(log_level=None):
    global log
    rlog = logging.getLogger()
    if __name__ == "__main__" and not rlog.hasHandlers():
        # setup root logger
        ch = logging.StreamHandler()
        formatter = logging.Formatter("%(levelname)s:%(name)s:: %(message)s")
        ch.setFormatter(formatter)
        rlog.addHandler(ch)

    log = logging.getLogger(__name__)

    if log_level is not None:
        log.setLevel(log_level)
        rlog.setLevel(log_level)
setup_log()

def main():
    def set_stdio_encoding(enc=NATIVE):
        import codecs; stdio = ["stdin", "stdout", "stderr"]
        for x in stdio:
            obj = getattr(sys, x)
            if not obj.encoding: setattr(sys, x, codecs.getwriter(enc)(obj))
    set_stdio_encoding()

    log_level = logging.INFO
    setup_log(log_level)

if __name__ == '__main__':
    main()

