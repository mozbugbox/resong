#!/usr/bin/python
# vim:fileencoding=utf-8:sw=4:et

from __future__ import print_function, unicode_literals, absolute_import
import sys
import os
import io
import logging as log

NATIVE=sys.getfilesystemencoding()

import gi
gi.require_versions({"Gtk": "3.0", "Gdk": "3.0", "PangoCairo": "1.0"})
from gi.repository import GLib, GObject, Gdk, Gtk
from gi.repository import Pango, PangoCairo
import cairo
import threading

GObject.threads_init()

from . import resong
from . import util

class TextImage:
    """Create icon image out of given text"""
    def __init__(self):
        self._test_cairo_gi()
        self.size = 64
        self.background = (1.0, 0.1, 0.1, 1.0) # solid red
        self.foreground = (1.0, 1.0, 0.0, 1.0)
        self.round_corner = True
        self.margin = -1
        self.fontname = "WenQuanYi Zen Hei"

    def _test_cairo_gi(self):
        from gi.repository import cairo as gicairo
        log.debug("gi.cairo: {}".format(gicairo._version))

    def draw(self, text="50", surface=None):
        """Draw text as image"""
        from gi.repository import Pango, PangoCairo
        import cairo
        # set icon image.
        size = self.size
        if surface is None:
            surface = cairo.ImageSurface(cairo.FORMAT_ARGB32, size*2, size*2)
        ctx = cairo.Context(surface)

        def draw_rounded(cr, area, radius):
            """ draws rectangles with rounded (circular arc) corners """
            from math import pi
            a,b,c,d=area
            cr.arc(a + radius, c + radius, radius, 2*(pi/2), 3*(pi/2))
            cr.arc(b - radius, c + radius, radius, 3*(pi/2), 4*(pi/2))
            cr.arc(b - radius, d - radius, radius, 0*(pi/2), 1*(pi/2))  # ;o)
            cr.arc(a + radius, d - radius, radius, 1*(pi/2), 2*(pi/2))
            cr.close_path()
            cr.fill()
            cr.stroke()

        ctx.set_source_rgba(*self.foreground)

        pango_layout = PangoCairo.create_layout(ctx)
        fd = Pango.FontDescription(self.fontname)
        if isinstance(text, bytes):
            text = text.decode("UTF-8")
        num_of_char = len(text)
        fd.set_size(size*Pango.SCALE/num_of_char)
        pango_layout.set_font_description(fd)
        pango_layout.set_text(text, -1)

        ink_extents, logic_extents = pango_layout.get_pixel_extents()
        ext = ink_extents

        text_size = size*2/3.0
        if self.margin >= 0:
            text_size = float(size)

        hscale = text_size/ext.width
        vscale = text_size/ext.height
        # more than 1 word, stretch vertically else keep aspect
        if (float(ext.height)/ext.width) > 0.67:
            vscale = min(hscale, vscale)
            hscale = vscale
        margin_x = (size - hscale*ext.width)/2.0
        margin_y = (size - vscale*ext.height)/2.0

        ctx.save()

        matrix = cairo.Matrix()
        matrix.translate(margin_x, margin_y)
        ctx.transform(matrix)
        matrix = cairo.Matrix()
        matrix.scale(hscale, vscale)

        ctx.transform(matrix)

        ctx.move_to(-ext.x, -ext.y)

        PangoCairo.update_layout(ctx, pango_layout)
        PangoCairo.show_layout(ctx, pango_layout)

        ctx.restore()
        ctx.fill()

        # Draw background
        ctx.set_operator(cairo.OPERATOR_DEST_OVER)
        ctx.set_source_rgba(*self.background)
        x = y = 0
        w = h = size
        if self.round_corner:
            draw_rounded(ctx, (x, x+w, y, y+h), min(margin_x, margin_y))
        else:
            ctx.rectangle(x, y, w, h)
            ctx.fill()

        pixbuf = Gdk.pixbuf_get_from_surface(surface, x, y, w, h)
        return pixbuf

class App(resong.App):
    def repeat_current_line(self, step):
        """Repeat the current lyric line by rewind it back according
        to lrc info.

        use threading to avoid stall on the GUI
        """
        s_info = self.get_song_info()
        if not s_info["title"]: return
        def _load_lyric_in_thread():
            """download lyric in a thread"""
            try:
                lrc = self.load_lrc(s_info["title"], s_info["singer"])
            except IOError as e:
                log.info('Failed to load lyric file "{}": {}'.format(
                    e.filename, e.strerror))
                return
            GObject.idle_add(self.do_repeat, s_info, lrc, step)
        t = threading.Thread(target=_load_lyric_in_thread)
        t.daemon = True
        t.start()

class GApp:
    """GUI application"""
    def __init__(self, options, app=None):
        self.win = None
        self.options = options
        if app is None:
            self.app = App(options)
        else:
            self.app = app
        self.repeat = 3

        self.load_ui()

    def load_ui(self):
        """ Load ui """
        GLib.set_prgname(resong.APPNAME)

        win = Gtk.Window()
        grid = Gtk.Grid()
        win.add(grid)

        # Setup toolbar and hotkeys
        action_entries = [
                ("Quit", Gtk.STOCK_QUIT, None, "q", "Quit - q ", self.quit),
                ("Rewind", Gtk.STOCK_MEDIA_REWIND, None,
                    "z", "Back Current Lyric - z ", self.on_rewind),
                ("Rewind2", Gtk.STOCK_MEDIA_PREVIOUS, None,
                    "x", "Back Previous Lyric - x ", self.on_rewind_2),
                ]

        action_toggle_entries = [
                ("Repeat", Gtk.STOCK_REFRESH, None, "r",
                    "Repeat {} times - r ".format(self.repeat),
                    self.on_repeat, False),
                ]

        ui_str = """
        <ui>
            <toolbar name="main-toolbar">
                <placeholder name="rewinds">
                    <toolitem name="Rewind" action="Rewind"/>
                    <toolitem name="Rewind2" action="Rewind2"/>
                    <toolitem name="Repeat" action="Repeat"/>
                </placeholder>
                <separator/>
                <toolitem name="Quit" action="Quit"/>
            </toolbar>
            <accelerator name="RewindAction" action="Rewind" />
            <accelerator name="Rewind2Action" action="Rewind2" />
            <accelerator name="RepeatAction" action="Repeat" />
            <accelerator name="QuitAction" action="Quit" />
        </ui>
        """
        actiongroup = Gtk.ActionGroup("Main")
        actiongroup.add_actions(action_entries)
        actiongroup.add_toggle_actions(action_toggle_entries)
        ui = Gtk.UIManager()
        ui.insert_action_group(actiongroup, 0)
        ui.add_ui_from_string(ui_str)

        toolbar = ui.get_widget("ui/main-toolbar")
        grid.attach(toolbar, 0, 0, 1, 1)

        accelgroup = ui.get_accel_group()
        win.add_accel_group(accelgroup)

        # restore last position
        left = top = -1
        if "left" in self.options:
            left = int(self.options["left"])
        if "top" in self.options:
            top = int(self.options["top"])
        if left >= 0 and top >= 0:
            win.move(left, top)

        ti = TextImage()
        pix = ti.draw("歌")
        win.set_icon(pix)
        win.set_title("再歌")

        win.set_decorated(False)
        win.show_all()

        win.add_events(Gdk.EventMask.BUTTON_PRESS_MASK)
        win.connect("delete-event", self.quit)
        win.connect("button-press-event", self.on_window_button_press)

        self.win = win
        self.ui = ui

    def save_position(self, cfile=None):
        """Save window position to config file"""
        if cfile is None and "config_file" in self.options:
            cfile = self.options["config_file"]

        if cfile is None or len(cfile) == 0:
            return


        sect = u"main"
        cfile = util.fix_path(cfile)

        import ConfigParser as configparser
        conf = configparser.ConfigParser()
        if os.path.exists(cfile):
            with io.open(cfile, "r", encoding="UTF-8") as fd:
                conf.readfp(fd)
        else:
            os.makedirs(os.path.dirname(cfile))
            conf.add_section(sect)

        left, top = self.win.get_position()
        conf.set(sect, u"left", str(left))
        conf.set(sect, u"top", str(top))
        with io.open(cfile, "wb") as fdw:
            conf.write(fdw)

    def on_rewind(self, *args):
        """Rewind current lyric"""
        self.set_repeat()
        self.app.repeat_current_line(0)

    def on_rewind_2(self, *args):
        """Rewind to previous lyric"""
        self.set_repeat()
        self.app.repeat_current_line(-1)

    def on_repeat(self, action):
        """nothing to do"""

    def set_repeat(self):
        """set initial repeat option value for the app"""
        action = self.ui.get_action("ui/RepeatAction")
        if action.props.active:
            self.app.options["repeat"] = self.repeat
        else:
            self.app.options["repeat"] = 1

    def on_window_button_press(self, w, evt):
        """Switch window decorator on/off on button event"""
        if evt.button == 3: # right button
            self.win.props.decorated = not self.win.props.decorated
            return True

    def run(self):
        import signal; signal.signal(signal.SIGINT, signal.SIG_DFL)
        print("Alt+left_mouse_button to move window.")
        Gtk.main()

    def quit(self, *args):
        """Quit action"""
        self.save_position()
        Gtk.main_quit()
        return True

def main():
    def set_stdio_encoding(enc=NATIVE):
        import codecs; stdio = ["stdin", "stdout", "stderr"]
        for x in stdio:
            obj = getattr(sys, x)
            if not obj.encoding: setattr(sys,  x, codecs.getwriter(enc)(obj))
    set_stdio_encoding()

    log_level = log.INFO
    log.basicConfig(format="%(levelname)s>> %(message)s", level=log_level)
    options = {"config_file": '${HOME}/.config/resong/config'}
    import ConfigParser as configparser
    conf = configparser.ConfigParser()
    cfile = options["config_file"]
    cfile = util.fix_path(cfile)
    if os.path.exists(cfile):
        with io.open(cfile, "r", encoding="UTF-8") as fd:
            conf.readfp(fd)
        options.update({x[0]:x[1] for x in conf.items("main")})

    print(options)
    app = GApp(options)
    app.run()

if __name__ == '__main__':
    main()

