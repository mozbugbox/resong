#!/usr/bin/python
# vim:fileencoding=utf-8:sw=4:et

from __future__ import print_function, unicode_literals, absolute_import
import sys
import os
import io
import logging as log

FNAME = "logo.png"

NATIVE=sys.getfilesystemencoding()

import resongGui
import cairo

def create_logo(txt, size):
    ti = resongGui.TextImage()
    ti.size = size
    surface = cairo.ImageSurface(cairo.FORMAT_ARGB32, size*2, size*2)
    ti.draw(txt, surface)
    return surface

def main():
    def set_stdio_encoding(enc=NATIVE):
        import codecs; stdio = ["stdin", "stdout", "stderr"]
        for x in stdio:
            obj = getattr(sys, x)
            if not obj.encoding: setattr(sys,  x, codecs.getwriter(enc)(obj))
    set_stdio_encoding()

    log_level = log.INFO
    log.basicConfig(format="%(levelname)s>> %(message)s", level=log_level)

    fname = FNAME
    surface = create_logo("歌", 256)
    print('Write logo to "{}"'.format(fname))
    surface.write_to_png(fname)

if __name__ == '__main__':
    main()

