#!/usr/bin/python3
# vim:fileencoding=utf-8:sw=4:et

from __future__ import print_function, unicode_literals, absolute_import, division
import sys
import os
import io
import logging

NATIVE=sys.getfilesystemencoding()

# URL: https://github.com/CamDavidsonPilon/Python-Numerics/tree/master/DamerauLevenshteinDistance
#
# Author: Michael Homer
# Date: Sunday, April 26th, 2009
# License: MIT
def dameraulevenshtein(seq1, seq2):
    """Calculate the Damerau-Levenshtein distance between sequences.

    This is used to match the best song searching result.

    This distance is the number of additions, deletions, substitutions,
    and transpositions needed to transform the first sequence into the
    second. Although generally used with strings, any sequences of
    comparable objects will work.

    Transpositions are exchanges of *consecutive* characters; all other
    operations are self-explanatory.

    This implementation is O(N*M) time and O(M) space, for N and M the
    lengths of the two sequences.

    >>> dameraulevenshtein('ba', 'abc')
    2
    >>> dameraulevenshtein('fee', 'deed')
    2

    It works with arbitrary sequences too:
    >>> dameraulevenshtein('abcd', ['b', 'a', 'c', 'd', 'e'])
    2
    """
    # codesnippet:D0DE4716-B6E6-4161-9219-2903BF8F547F
    # Conceptually, this is based on a len(seq1) + 1 * len(seq2) + 1 matrix.
    # However, only the current and two previous rows are needed at once,
    # so we only store those.
    if seq1 == seq2: return 0
    oneago = None
    thisrow = list(range(1, len(seq2) + 1)) + [0]
    for x in range(len(seq1)):
        # Python lists wrap around for negative indices, so put the
        # leftmost column at the *end* of the list. This matches with
        # the zero-indexed strings and saves extra calculation.
        twoago, oneago, thisrow = oneago, thisrow, [0] * len(seq2) + [x + 1]
        for y in range(len(seq2)):
            delcost = oneago[y] + 1
            addcost = thisrow[y - 1] + 1
            subcost = oneago[y - 1] + (seq1[x] != seq2[y])
            thisrow[y] = min(delcost, addcost, subcost)
            # This block deals with transpositions
            if (x > 0 and y > 0 and seq1[x] == seq2[y - 1]
                and seq1[x-1] == seq2[y] and seq1[x] != seq2[y]):
                thisrow[y] = min(thisrow[y], twoago[y - 2] + 1)
    return thisrow[len(seq2) - 1]

def guess_best_match(lyric_list, title, singer):
    """From a list of lyric_info, guess the best match for the
    @title and @singer values.
    @return: a lyric info dict for the best fit or None if no one found"""
    title = title.lower()
    def remove_symbols(txt):
        symbols = "`',.~&"
        for s in symbols :
            txt = txt.replace(s, "")
        return txt
    title = remove_symbols(title)
    exact_match = []
    for i in range(len(lyric_list)):
        l = lyric_list[i]
        ltitle = l["歌曲"].lower()
        ltitle = remove_symbols(ltitle)
        if ltitle == title:
            exact_match.append(l)
    if len(exact_match) > 0:
        lyric_list = exact_match

    if singer is not None:
        singer_list = singer.lower().split()

        def str_metric(x):
            x_list = x.lower().split()
            dists = [dameraulevenshtein(s,x)
                        for x in x_list
                        for s in singer_list]
            dists.sort()
            log.debug("dist :{}:{}:{}".format(singer, x, dists))
            return dists
        lyric_list.sort(key=lambda x:str_metric(x.get("歌手","")))
    log.debug("Lyric info list: {}".format(lyric_list))
    best_fit = None
    for linfo in lyric_list:
        if "url" in linfo:
            best_fit = linfo
            break
    return best_fit

def setup_log(log_level=None):
    global log
    rlog = logging.getLogger()
    if __name__ == "__main__" and not rlog.hasHandlers():
        # setup root logger
        ch = logging.StreamHandler()
        formatter = logging.Formatter("%(levelname)s:%(name)s:: %(message)s")
        ch.setFormatter(formatter)
        rlog.addHandler(ch)

    log = logging.getLogger(__name__)

    if log_level is not None:
        log.setLevel(log_level)
        rlog.setLevel(log_level)
setup_log()

def main():
    def set_stdio_encoding(enc=NATIVE):
        import codecs; stdio = ["stdin", "stdout", "stderr"]
        for x in stdio:
            obj = getattr(sys, x)
            if not obj.encoding: setattr(sys, x, codecs.getwriter(enc)(obj))
    set_stdio_encoding()

    log_level = logging.INFO
    setup_log(log_level)

if __name__ == '__main__':
    main()

