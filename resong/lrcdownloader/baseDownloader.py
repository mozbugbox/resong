#!/usr/bin/python3
# vim:fileencoding=utf-8:sw=4:et

from __future__ import print_function, unicode_literals, absolute_import, division
import sys
import os
import io
import logging

try:
    import urllib.parse as urlparse
    import urllib.request as request
    from urlparse import quote_plus
except ImportError:
    import urllib2 as request
    import urlparse
    from urllib import quote_plus

NATIVE=sys.getfilesystemencoding()

class BaseDownloader:
    # http://music.baidu.com/search/lrc?key=when+you+say+nothing+at+all+-+ronan+keating
    url_template = "http://music.baidu.com/search/lrc?key={}"
    tags = {"歌手", "歌曲", "专辑"}
    def __init__(self):
        """Download LRC from the net"""

    def get_lrc_info_list(self, title, singer):
        """Get a list of lrc info"""
        # Search title + singer *could* get more precise results
        # or get zero result.
        lookup_keys = [title]
        if singer is not None:
            song_key = " - ".join([title, singer])
            lookup_keys.insert(0, song_key)

        for lkey in lookup_keys:
            url = self.setup_url(lkey)
            log.debug("loading front face: {}".format(url))
            content = self.fetch(url)
            lyric_list = self.collect_lyric_infos(content, url)
            if len(lyric_list) > 0:
                break
        return lyric_list

    def download(self, title, singer=None):
        """Download lrc by title and singer"""
        lyric_list = self.get_lrc_info_list(title, singer)

        if len(lyric_list) == 0:
            return None
        lrc_url = lyric_list[0]["url"]
        lrc = self.get_lrc(lrc_url)
        return lrc

    def get_lrc(self, url):
        log.debug("Loading lrc content: {}".format(url))
        lrc_text = self.fetch(url)
        return lrc_text

    def setup_url(self, title):
        """compose a search url for the given song title"""
        title = title.encode("UTF-8")
        title_u = quote_plus(title)
        log.debug("URL quoted title: {}".format(title_u))
        url = self.url_template.format(title_u)
        return url

    def fetch(self, url, encoding=None):
        """utility func to down a url"""
        fd = request.urlopen(url, timeout=60)
        content = fd.read()
        fd.close()
        if encoding is None:
            encoding = "UTF-8"
        try:
            content = content.decode(encoding)
        except UnicodeDecodeError:
            content = content.decode("GB18030")
        return content

    def collect_lyric_infos(self, html_txt, url):
        """parse search page to collect lyric link infomation"""
        raise NotImplemented

def setup_log(log_level=None):
    global log
    rlog = logging.getLogger()
    if __name__ == "__main__" and not rlog.hasHandlers():
        # setup root logger
        ch = logging.StreamHandler()
        formatter = logging.Formatter("%(levelname)s:%(name)s:: %(message)s")
        ch.setFormatter(formatter)
        rlog.addHandler(ch)

    log = logging.getLogger(__name__)

    if log_level is not None:
        log.setLevel(log_level)
        rlog.setLevel(log_level)
setup_log()

def main():
    def set_stdio_encoding(enc=NATIVE):
        import codecs; stdio = ["stdin", "stdout", "stderr"]
        for x in stdio:
            obj = getattr(sys, x)
            if not obj.encoding: setattr(sys, x, codecs.getwriter(enc)(obj))
    set_stdio_encoding()

    log_level = logging.INFO
    setup_log(log_level)

if __name__ == '__main__':
    main()

