#!/usr/bin/python
# vim:fileencoding=utf-8:sw=4:et

from __future__ import print_function, unicode_literals, absolute_import, division
import sys
import os
import io
import logging

import re
try:
    import urllib.parse as urlparse
except ImportError:
    import urlparse

from . import util

NATIVE=sys.getfilesystemencoding()

def get_prev_text(elm):
    """Get text in the front of the tag"""
    #print(html.tostring(elm))
    prev = elm.getprevious()
    if prev is None:
        prev = elm.getparent()
    #print(html.tostring(prev))
    ptext = prev.text
    if ptext is None or ptext.isspace():
        ptext = prev.tail
    #print(ptext)
    return ptext

from .baseDownloader import BaseDownloader
class Lrc123Downloader(BaseDownloader):
    # http://www.lrc123.com/?keyword=when+you+say+nothing+at+all&field=all
    url_template = "http://www.lrc123.com/?keyword={}&field=all"
    tags = {"歌手", "歌曲", "专辑"}

    def download(self, title, singer=None):
        """Download lrc by title and singer"""

        lyric_list = self.get_lrc_info_list(title, singer)
        if len(lyric_list) == 0:
            return None
        best_lyric = util.guess_best_match(lyric_list, title, singer)
        lrc_url = best_lyric["url"]
        if best_lyric is None:
            song_tag = "{} - {}".format(singer, title)
            log.info('Failed to find a proper lyric download link for' +
                    ' "{}"'.format(song_tag))
            return None
        lrc = self.get_lrc(lrc_url)
        return lrc

    def collect_lyric_infos(self, html_txt, url):
        """parse search page to collect lyric link infomation"""
        #html_txt = html_txt.replace('class="mr"href', 'class="mr" href')
        result = []
        import lxml.html as html
        xml = html.fromstring(html_txt, base_url=url)
        lyrics = xml.xpath('.//div[@class="newscont mb15"]')
        if not lyrics:
            return None

        for lyric_div in lyrics:
            info = {}
            anchors = lyric_div.xpath('.//a[@class="mr"]')
            for anchor in anchors:
                txt = get_prev_text(anchor).strip().rstrip(":")
                if txt in self.tags:
                    c = html.tostring(anchor, method="text", encoding="UTF-8")
                    info[txt] = c.decode("UTF-8")

            anchors = lyric_div.xpath(".//a")
            for anchor in anchors:
                aurl = anchor.get("href")
                if "/download/lrc" in aurl:
                    info["url"] = urlparse.urljoin(url, aurl)
                    break
            if len(info) != 0:
                result.append(info)
        #print(result)
        return result

def setup_log(log_level=None):
    global log
    rlog = logging.getLogger()
    if __name__ == "__main__" and not rlog.hasHandlers():
        # setup root logger
        ch = logging.StreamHandler()
        formatter = logging.Formatter("%(levelname)s:%(name)s:: %(message)s")
        ch.setFormatter(formatter)
        rlog.addHandler(ch)

    log = logging.getLogger(__name__)

    if log_level is not None:
        log.setLevel(log_level)
        rlog.setLevel(log_level)
setup_log()

def main():
    def set_stdio_encoding(enc=NATIVE):
        import codecs; stdio = ["stdin", "stdout", "stderr"]
        for x in stdio:
            obj = getattr(sys, x)
            if not obj.encoding: setattr(sys, x, codecs.getwriter(enc)(obj))
    set_stdio_encoding()

    log_level = logging.INFO
    setup_log(log_level)

if __name__ == '__main__':
    main()

