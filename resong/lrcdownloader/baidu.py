#!/usr/bin/python3
# vim:fileencoding=utf-8:sw=4:et

from __future__ import print_function, unicode_literals, absolute_import, division
import sys
import os
import io
import logging

import re
try:
    import urllib.parse as urlparse
    import urllib.request as request
except ImportError:
    import urllib2 as request
    import urlparse

from . import util

NATIVE=sys.getfilesystemencoding()

from .baseDownloader import BaseDownloader
class BaiduDownloader(BaseDownloader):
    # http://music.baidu.com/search/lrc?key=when+you+say+nothing+at+all+-+ronan+keating
    url_template = "http://music.baidu.com/search/lrc?key={}"
    tags = {"歌手", "歌曲", "专辑"}

    def collect_lyric_infos(self, html_txt, url):
        """parse search page to collect lyric link infomation"""
        #html_txt = html_txt.replace('class="mr"href', 'class="mr" href')
        result = []
        import lxml.html as html
        xml = html.fromstring(html_txt, base_url=url)
        lyrics = xml.xpath(
                './/'
                'span[@class="lyric-action"]/'
                'a[contains(@class, "down-lrc-btn")]')
        if not lyrics:
            return None

        href_re = re.compile("(?:href':')([^']+)")
        for lyric_div in lyrics:
            info = {}
            aclass = lyric_div.get("class")
            m = href_re.search(aclass)
            if m:
                url_path = m.group(1)
                url = urlparse.urljoin(url, url_path)
                log.debug("lrc URL: %s", url)
                info['url'] = url

            if len(info) != 0:
                result.append(info)
        #print(result)
        return result

def setup_log(log_level=None):
    global log
    rlog = logging.getLogger()
    if __name__ == "__main__" and not rlog.hasHandlers():
        # setup root logger
        ch = logging.StreamHandler()
        formatter = logging.Formatter("%(levelname)s:%(name)s:: %(message)s")
        ch.setFormatter(formatter)
        rlog.addHandler(ch)

    log = logging.getLogger(__name__)

    if log_level is not None:
        log.setLevel(log_level)
        rlog.setLevel(log_level)
setup_log()

def main():
    def set_stdio_encoding(enc=NATIVE):
        import codecs; stdio = ["stdin", "stdout", "stderr"]
        for x in stdio:
            obj = getattr(sys, x)
            if not obj.encoding: setattr(sys, x, codecs.getwriter(enc)(obj))
    set_stdio_encoding()

    log_level = logging.INFO
    log_level = logging.DEBUG
    setup_log(log_level)

    dler = BaiduDownloader()
    lrc = dler.download("when you say nothing at all", "ronan keating")
    print(lrc)

if __name__ == '__main__':
    main()

